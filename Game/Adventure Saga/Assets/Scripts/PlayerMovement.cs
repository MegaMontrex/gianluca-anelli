﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	public int playerSpeed = 10;
	private bool facingRight = true;
	public int playerJumpPower = 1000;
	private float moveX;
	public GameObject Laze, LazeRight;
	Transform Lazer;



	void Start(){
		Lazer = transform.Find ("Lazer");
	}
		
	// Update is called once per frame
	void Update () {
		PlayerMove();
		if (Input.GetKeyDown (KeyCode.F)) {
			Fire ();
		}
	}

	void PlayerMove(){
		moveX = Input.GetAxis ("Horizontal");
		if (Input.GetButtonDown ("Jump")){
			Jump ();
		}

		if (moveX < 0.0f && facingRight == true) {
			FlipPlayer ();
		} 
		else if (moveX > 0.0f && facingRight == false) {
			FlipPlayer ();
		}
		gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector3 (moveX * playerSpeed, gameObject.GetComponent<Rigidbody2D> ().velocity.y);
	}

	void Jump(){
		GetComponent<Rigidbody2D> ().AddForce (Vector3.up * playerJumpPower);

	}

	void FlipPlayer(){
		facingRight = !facingRight;
		Vector3 localScle = gameObject.transform.localScale;
		localScle.x *= -1;
		transform.localScale = localScle;
	}

	void Fire(){
		if (facingRight) 
			Instantiate (LazeRight, Lazer.position, Quaternion.identity);
		if(!facingRight)
			Instantiate (Laze, Lazer.position, Quaternion.identity);
		
	}



}
