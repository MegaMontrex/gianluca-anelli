﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerScore : MonoBehaviour {


	private float timeLeft = 120;
	public int playerScore = 0;
	public GameObject timerLeftUI;

	// Update is called once per frame
	void Update () {
		timeLeft -= Time.deltaTime;
		timerLeftUI.gameObject.GetComponent<Text> ().text = ("Time Left:" + timeLeft);
		if (timeLeft < 0.1f) {
			SceneManager.LoadScene ("MainCamera");
		}
	}
}
