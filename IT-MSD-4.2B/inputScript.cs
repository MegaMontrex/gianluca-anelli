﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class inputScript : MonoBehaviour {
	InputField nameField,surnameField,ageField;
	Toggle licenseToggle;
	Slider heightSlider;
	Button addButton;
	Text heightText;

	Person tempPerson;

	// Use this for initialization
	void Start () {
		nameField = GameObject.Find ("nameInputField").GetComponent<InputField> ();
		surnameField = GameObject.Find ("surnameInputField").GetComponent<InputField> ();
		ageField = GameObject.Find ("ageInputField").GetComponent<InputField> ();
		licenseToggle = GameObject.Find ("licenseToggle").GetComponent<Toggle> ();
		heightSlider = GameObject.Find ("heightSlider").GetComponent<Slider> ();
		heightText = GameObject.Find ("heightText").GetComponent<Text> ();
		addButton = GameObject.Find ("addButton").GetComponent<Button> ();

		tempPerson = new Person ();

		licenseToggle.onValueChanged.AddListener(value => {
			licenseToggleToggled(licenseToggle.isOn);
		});


		heightSlider.onValueChanged.AddListener (value => {
			heightSliderSlid(heightSlider.value);
		});

		addButton.onClick.AddListener (addButtonPressed);

	}

	void addButtonPressed()
	{

	}

	void licenseToggleToggled(bool currentValue)
	{
		//save the toggle value in tempPerson
		tempPerson.hasDrivingLicense = currentValue;
	}

	void heightSliderSlid(float currentValue)
	{
		heightText.text = heightSlider.value.ToString();
		tempPerson.height = heightSlider.value;
	}

	
	// Update is called once per frame
	void Update () {
		
	}
}
